# Chat client
> A light weight and simple to use chat client coded by Christof Weickhardt in Java 1.8
## Task
The pdf containing the general task and the scale for grading this project is included in the ```task``` folder
## Design
### UI
As a part of the ui a sketch of the demanded features has been included in the `design` folder. As a part of the Java implementation `Swing` and its `GridBagLayout` is used
### UX
>UX is like a joke if you have to explain it its bad

The general rule for UX is to keep it simple. Therefore every input needed to send a message and select recipients should be reachable next to each other.

## Architecture
As described in the task we use the MVC pattern. Where a specific controllers control alls of views, models and services.
### Services
This project contains two general purpose services. `SendService` and  ```ReceiveService``` which are consumed by the Messaging Service which detects different events and executes application consumable callbacks.
### Views
Views are split into two categories `Frames` and `Panels`. This represents the basic structure of `JFrame` and `JPanel` and serve as reusable components. Frames can consume panels but all of them are controlled by a specified controller. Views contain no business intelligence but code to style and manipulate the UI.
### Folder tree
```
.
├── src
└── chat
├── controllers
├── helpers
├── interfaces
├── main
├── models
├── services
└── views
├── frames
└── panels
```
## Configuration
This application uses the DotEnv aproche. Details such as the username, server address and port is stored in a `.env` file in the users home directory if possible or in the application root folder. If it's not possible to store any of them default values are provided as in the tasks pdf
## Network communication
General network notifications are sent by the `SendService` and received by the `ReceiveService` the both have their own Thread to work within and use concurrent messaging to reduce CPU usage. Alls of the objects are serializable so they remain in their format when received
### Protocol
The main messaging protocol has been added as `projects/src/lib/m120-ChatProtocol.jar` and implemented throughout the project.
## Code
> The source code is provided in the project folder. The Main entrace is in the package `main` in the `main.class`
### Logging
All actions performed are tracable via the console output
### Documentation
Documentation can be found throughout  the source files. The comments are part of the java doc which can be generated as html which is provided in the `documentation` folder
### Tests
Since testing isn't a part of this modules scope and not specified in the tasks no trivial unit tests have been done
