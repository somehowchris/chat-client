/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.models;

import java.util.Date;

/**
 *
 * @author Christof Weickhardt
 */
public class Message {
    
    private Date date;
    private String user;
    private String message;
    
    public Message(){}
    
    public Message(String input){
        date = new Date();
        user = input.split(":")[0];
        message = input.replaceAll(user, "");
    }

    public Message(Date date, String user, String message) {
        this.date = date;
        this.user = user;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

}
