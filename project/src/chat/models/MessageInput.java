/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.models;

/**
 *
 * @author Christof Weickhardt
 */
public class MessageInput {
    
    private String message;

    public MessageInput(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    
}
