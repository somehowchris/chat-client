/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.models;

/**
 *
 * @author Christof Weickhardt
 */
public class Config {
    
    private String username;
    private String password;
    private String address;

    public Config() {
    }

    public Config(String username, String password, String address) {
        this.username = username;
        this.password = password;
        this.address = address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
    
}
