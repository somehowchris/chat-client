/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.views.panels;

import chat.interfaces.SendMessageCallback;
import chat.services.SendService;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Christof Weickhardt
 */
public class MessageInputModel extends JPanel {
    
    JTextArea input;
    JButton send;
    JButton clear;
    SendMessageCallback sender;
    
    public MessageInputModel(SendMessageCallback sender){
        this.sender = sender;
        this.clear = new JButton("Clear History");
        setLayout(new GridBagLayout());
        input = new JTextArea("Let's send a message...");
        send = new JButton("Send");
        Insets defaultInsets = new Insets(0, 8, 8, 8);
        JScrollPane sp = new JScrollPane(input);
        this.add(send, new GridBagConstraints(15, 1, 1, 1, 0.05, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, defaultInsets, 0, 0));
        this.add(sp,new GridBagConstraints(0, 0, 15, 2, 0.95, 1, GridBagConstraints.EAST, GridBagConstraints.BOTH, defaultInsets, 0, 0));
        this.add(clear, new GridBagConstraints(15, 0, 1, 1, 0.05, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, defaultInsets, 0, 0));
        input.setSize(200, 100);
        input.setWrapStyleWord(true);
        input.setLineWrap(true);
        input.setBorder(BorderFactory.createEmptyBorder());
        sp.setBorder(BorderFactory.createEmptyBorder());
        input.setAlignmentY(TOP_ALIGNMENT);
    }

    public JTextArea getInput() {
        return input;
    }

    public JButton getSend() {
        return send;
    }
}
