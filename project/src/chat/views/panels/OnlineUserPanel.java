/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.views.panels;

import chat.interfaces.UserSelectCallback;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.function.Consumer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 *
 * @author Christof Weickhardt
 */
public class OnlineUserPanel extends JPanel {
   JList<String> listofUsers;
   UserSelectCallback callback;
   public OnlineUserPanel(UserSelectCallback callback){
       setLayout(new GridBagLayout());
       listofUsers = new JList<String>();
       listofUsers.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
       Insets defaultInsets = new Insets(0, 0, 0, 0);
       
       add(listofUsers, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, defaultInsets, 0, 0));
       this.setBackground(Color.WHITE);
   }
   
   /**
    * Updates the list of users in the jlist
    */
   public void updateList(List<String> users) {
        DefaultListModel model = new DefaultListModel();
        if(users != null){
            users.forEach(new Consumer<String>() {
                @Override
                public void accept(String t) {
                    model.addElement(t);
                }
            });
        }
        this.listofUsers.setModel(model);
    }

    public JList<String> getListofUsers() {
        return listofUsers;
    }
}
