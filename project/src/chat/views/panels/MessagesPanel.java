/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.views.panels;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Christof Weickhardt
 */
public class MessagesPanel extends JPanel {
    JTextPane textPane;
    ArrayList<String> messages;
    public MessagesPanel(){
        messages = new ArrayList<>();
        textPane = new JTextPane();
        DefaultListModel model = new DefaultListModel();
        textPane.setSize(200,200);
        setLayout(new GridBagLayout());
        JScrollPane sp = new JScrollPane(textPane);
        textPane.setEditable(false);
        textPane.setBorder(null);
        Insets defaultInsets = new Insets(0, 0, 0, 0);
        add(sp, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.BOTH, defaultInsets, 0, 0));
        sp.setBorder(BorderFactory.createEmptyBorder());
        sp.setBackground(Color.WHITE);
    }
    
    /**
     * Adds a message to the list
     * @param message Message to add
     */
    public void addMessages(String message) throws BadLocationException{
        SimpleAttributeSet boldWord = new SimpleAttributeSet();
        StyleConstants.setBold(boldWord, true);
        SimpleAttributeSet normalWord = new SimpleAttributeSet();
        StyleConstants.setBold(normalWord, false);
        StyledDocument doc = textPane.getStyledDocument();
        String username = "@"+message.split(" : ")[0];
        message = ": "+message.split(" : ")[1];
        
        if(doc.getLength() > 0){
            message = message+"\n";
        }
        
        doc.insertString(0,username, boldWord);
        doc.insertString(username.length(),message, normalWord);
    }
}
