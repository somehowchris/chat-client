package chat.views.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.*;
import java.awt.Insets;
/**
 *
 * @author Christof Weickhardt
 */
public class LoginFrame extends JFrame {
  private JButton blogin;
  private JPanel loginpanel;
  private JTextField txuser;
  private JTextField serverAdress;
  public JTextField pass;
  private JLabel username;
  private JLabel password;
  private JLabel txserverAdress;


  public LoginFrame(){
    super("Login Autentification");

    blogin = new JButton("Login");
    loginpanel = new JPanel();
    txuser = new JTextField(15);
    pass = new JPasswordField(15);
    username = new JLabel("Username");
    password = new JLabel("Password");
    serverAdress = new JTextField();
    txserverAdress = new JLabel("Server Adress");

    loginpanel.setLayout(new GridBagLayout()); 

    txuser.setBounds(70,30,150,20);
    pass.setBounds(70,65,150,20);
    blogin.setBounds(110,100,80,20);
    username.setBounds(20,28,80,20);
    password.setBounds(20,63,80,20);
    serverAdress.setBounds(70,65,150,20);
    
    Insets labelInsets = new Insets(0, 12, 0, 0);
    Insets zeroInsets = new Insets(0, 0, 0, 0);

    loginpanel.add(blogin, new GridBagConstraints(0, 3, 2, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, zeroInsets, 0, 0));
    loginpanel.add(txuser, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, zeroInsets, 0, 0));
    loginpanel.add(pass, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, zeroInsets, 0, 0));
    loginpanel.add(username, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, labelInsets, 0, 0));
    loginpanel.add(password, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, labelInsets, 0, 0));
    loginpanel.add(serverAdress, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, zeroInsets, 0, 0));
    loginpanel.add(txserverAdress, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, labelInsets, 0, 0));

    this.getContentPane().add(loginpanel);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);
    this.setResizable(false);
    this.setSize(320,200);
    this.setLocation(500,280);
  } 

    public JButton getLoginButton() {
        return blogin;
    }

    public JPanel getLoginPanel() {
        return loginpanel;
    }

    public JTextField getUsernameField() {
        return txuser;
    }

    public JTextField getServerAdressField() {
        return serverAdress;
    }

    public JTextField getPasswordField() {
        return pass;
    }
}