/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.views.frames;

import chat.controllers.MessageController;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import chat.views.panels.*;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.io.IOException;
/**
 *
 * @author Christof Weickhardt
 */

public class MessageFrame extends JFrame {
    private MessageInputModel input;
    private MessagesPanel messages;
    private OnlineUserPanel users;
    
    JPanel panel;
    
    public MessageFrame(MessageController controller) throws IOException{
        super("Chat client");
        panel = new JPanel();
        input = new MessageInputModel(controller);
        messages = new MessagesPanel();
        users = new OnlineUserPanel(controller);
         
        panel.setLayout(new GridBagLayout());
        panel.setLayout(new GridBagLayout()); 
        
        Insets defaultInsets = new Insets(8, 8, 0, 8);
        Insets zeroInsets = new Insets(8, 0, 0, 0);
        panel.add(messages, new GridBagConstraints(0, 0, 3, 1, 0.75, 0.99, GridBagConstraints.NORTH, GridBagConstraints.BOTH, defaultInsets, 0, 0));
        panel.add(input, new GridBagConstraints(0, 1, 4, 1, 1, 0.01, GridBagConstraints.LAST_LINE_END, GridBagConstraints.HORIZONTAL, zeroInsets, 0, 0));
        panel.add(users, new GridBagConstraints(3, 0, 1, 1, 0.25, 0.99, GridBagConstraints.NORTH, GridBagConstraints.BOTH, defaultInsets, 0, 0));
           
        this.add(panel);
        this.setSize(880,720);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
    }

    public MessagesPanel getMessages() {
        return messages;
    }

    public OnlineUserPanel getUsers() {
        return users;
    }

    public MessageInputModel getInput() {
        return input;
    }
    
    
}
