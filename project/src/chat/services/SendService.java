/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.services;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import m120.ChatProtocol.ToServer;

/**
 *
 * @author Christof Weickhardt
 */
public class SendService extends Thread{
    
    private ObjectOutputStream outStream;
    private BlockingQueue<ToServer> queue;

    public SendService(Socket s) throws IOException{
        queue = new LinkedBlockingQueue<>();
        outStream = new ObjectOutputStream(s.getOutputStream());
        outStream.flush();
    }

    /**
     * Public visible send method which adds it to the queue
     * @param obj Object to send
     */
    public void sendMessage(ToServer obj){
        queue.add(obj);
    }
    
    /**
     * Writing the object to the stream and flushing all of it
     */
    private synchronized void send(Object obj){
        try {
            if(obj != null){
                outStream.writeObject(obj);
                outStream.flush();
            }
        } catch (IOException ex) {}
    }
    
    /**
     * Thread runner to wait for a object in the queue and send it
     */
    public void run(){
        while(true){
            ToServer message = null;
            try {
                message = queue.take();
                send(message);
            } catch (Exception ex) {
            }
        }
    }

}
