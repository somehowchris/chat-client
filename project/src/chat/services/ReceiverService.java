/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.services;

import chat.interfaces.CallbackInterface;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import m120.ChatProtocol.ToClient;

/**
 *
 * @author Christof Weickhardt
 */
public class ReceiverService extends Thread{

    
    private CallbackInterface callback;
    private volatile ObjectInputStream inStream;

    public ReceiverService(Socket socket, CallbackInterface callback) throws IOException {
        this.callback = callback;
        this.inStream = new ObjectInputStream(socket.getInputStream());
    }

    /**
     * Thred runner to read objects and forwared them to the callback
     */
    @Override
    public void run() {
        while(true){
            try {
                Object obj = inStream.readObject();
                if(obj != null && obj instanceof ToClient){
                    callback.onCallback(obj);
                }
            } catch (IOException ex) {} catch (ClassNotFoundException ex) {}
        }
    }
    
    
    
}
