/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.interfaces;

/**
 *
 * @author Christof Weickhardt
 */
public interface CallbackInterface<T> {
    
    /**
     * General callback
     * @param obj Object to parse
     */
    public void onCallback(T obj);
}
