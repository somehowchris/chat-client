/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.interfaces;

import java.util.List;

/**
 *
 * @author Christof Weickhardt
 */
public interface UserSelectCallback {
    /**
     * Callback to receive a list of selected usernames
     * @param users List of usernames
     */
    public void onSelectUser(List<String> users);
}
