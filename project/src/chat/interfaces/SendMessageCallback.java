/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.interfaces;

/**
 *
 * @author Christof Weickhardt
 */
public interface SendMessageCallback {
    /**
     * Callback to receive the send action
     * @param text Text to send
     */
    public void sendMessageCallback(String text);
}
