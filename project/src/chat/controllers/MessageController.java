/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.controllers;

import chat.interfaces.CallbackInterface;
import chat.interfaces.SendMessageCallback;
import chat.interfaces.UserSelectCallback;
import chat.models.Config;
import chat.services.SendService;
import chat.views.frames.MessageFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import m120.ChatProtocol.MessageToClients;
import m120.ChatProtocol.MessageToServer;
import m120.ChatProtocol.OnlineUserList;
import m120.ChatProtocol.ToClient;

/**
 *
 * @author Christof Weickhardt
 */
public class MessageController implements CallbackInterface<ToClient>, UserSelectCallback, SendMessageCallback {
    
    Config config;
    Socket socket;
    SendService send;
    MessageFrame frame;
    
    List<String> users;

    public MessageController() {
    }

    public MessageController(Config config, Socket socket, SendService sendService) throws IOException {
        this.config = config;
        this.socket = socket;
        send = sendService;
        
        this.frame = new MessageFrame(this);
        users = new ArrayList<>();
        
        // listener to get the selected users
        this.frame.getUsers().getListofUsers().addListSelectionListener(new ListSelectionListener() {
           @Override
           public void valueChanged(ListSelectionEvent e) {
               onSelectUser(frame.getUsers().getListofUsers().getSelectedValuesList());
           }
        });
        
        // listener to get the send action
        this.frame.getInput().getSend().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMessageCallback(frame.getInput().getInput().getText());
                frame.getInput().getInput().setText("Let's send a message...");
            }
        });
        validateToSend();
    }

    /**
     * General callback to get messages received and performed the correct action
     * @param obj General object received
     */
    @Override
    public synchronized void onCallback(ToClient obj) {
        if(obj instanceof MessageToClients) {
            try {
                this.frame.getMessages().addMessages(((MessageToClients) obj).getMessage());
            } catch (BadLocationException ex) {
            }
        }
        if(obj instanceof OnlineUserList) {
            List list = Arrays.asList(((OnlineUserList) obj).usernames.toArray());
            this.frame.getUsers().updateList(list);
        }
    }

    /**
     * Callback for userselection of subcomponent
     * @param users List of username
     */
    @Override
    public void onSelectUser(List<String> users) {
        this.users = users;
        if(this.users.contains(config.getUsername())){
            this.users.remove(config.getUsername());
        }
        validateToSend();
    }
    
    /**
     * Validation method to lock or unlock the send button
     */
    public void validateToSend() {
        this.frame.getInput().getSend().setEnabled(this.users.size() > 0 && this.frame.getInput().getInput().getText().trim().length() > 0);
    }

    
    /**
     * Send callback if a user wants to send a message 
     */
    @Override
    public void sendMessageCallback(String text) {
        if(!this.users.isEmpty() && !text.isEmpty()) {
            MessageToServer message = new MessageToServer();
            Vector<String> u = new Vector<String>();
            for(Object o : this.users)
                u.add(o.toString());
            u.add(config.getUsername());
            message.setUsernames(u);
            message.setMessage(text);
            message.setUserPassword(config.getUsername(), config.getPassword());
            send.sendMessage(message);
        }
    }
    
    
}
