/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.controllers;

import chat.helpers.DotEnv;
import chat.models.Config;
import chat.services.ReceiverService;
import chat.services.SendService;
import chat.views.frames.LoginFrame;
import java.awt.event.ActionEvent;
import java.net.Socket;
import java.util.HashMap;
import m120.ChatProtocol.ClientLogon;

/**
 *
 * @author Christof Weickhardt
 */
public class LoginController {
    
    LoginFrame frame;

    public LoginController() {
        this.frame = new LoginFrame();
        addLoginListener();
        readDotEnv();
    }
    
    /**
     * Reads the dot env file and checks for possible inputs
     */
    public void readDotEnv() {
        HashMap dotEnv = DotEnv.getDotEnv();
        
        String username = (String) dotEnv.get("CHAT_USERNAME");
        String address = (String) dotEnv.get("CHAT_ADDRESS");
        
        this.frame.getUsernameField().setText(username);
        this.frame.getServerAdressField().setText(address);
        
    }
    
    /**
     * Registers the listener on the login button and includes the logic to check if a user is autherized or not
     * If so the user will be forwareded to the main application window to send messages
     */
    public void addLoginListener(){
        this.frame.getLoginButton().addActionListener((ActionEvent e) -> {
            String username = frame.getUsernameField().getText().trim();
            String password = frame.getPasswordField().getText().trim();
            String serverAddress = frame.getServerAdressField().getText().trim();
            String host = serverAddress;
            int port = 2002;
            if(!serverAddress.isEmpty() && serverAddress.contains(":")) {
                try{
                    host = serverAddress.split(":")[0];
                    port = new Integer(serverAddress.split(":")[1]);
                } catch (Exception ex){
                    return;
                }
            }
            
            Config config = new Config(username, password, serverAddress);
            
            
            Socket s;
            try {
                s = new Socket(host, port);
                SendService send = new SendService(s);
                MessageController message = new MessageController(config, s, send);
                ReceiverService receive = new ReceiverService(s, message);
                send.start();
                receive.start();
                message.frame.setVisible(false);
                
                ClientLogon logon = new ClientLogon();
                logon.setUserPassword(config.getUsername(), config.getPassword());
                send.sendMessage(logon);
                
                
                // Server should close the conntection if not autherized
                if(s.isClosed()){
                    System.out.println("User not autherized");
                    return;
                }
                
                // Sets and saved the dot env
                HashMap dotEnv = DotEnv.getDotEnv();
                
                dotEnv.put("CHAT_USERNAME", config.getUsername());
                dotEnv.put("CHAT_ADDRESS", config.getAddress());
                
                DotEnv.setDotEnv(dotEnv);
                
                frame.setVisible(false);
                message.frame.setVisible(true);
            } catch (Exception ex) {}
        });
    }
}
